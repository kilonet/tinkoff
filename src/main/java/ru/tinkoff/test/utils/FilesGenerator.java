package ru.tinkoff.test.utils;

import java.io.BufferedWriter;
import java.io.Console;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Paths;
import java.util.Random;

import lombok.SneakyThrows;

/**
 * utility for generating test data
 * @author pokrevsky-a-d
 *         date 17.09.2018
 */
public class FilesGenerator {

    public static final int MARK = 10_000_000; //10000000

    public static void main(String[] args) {
        System.out.println("generating test data");
        generateTestData();
    }

    public static void generateTestData() {
        File filesDir = new File("files");

        if (filesDir.isDirectory()) {
            System.out.println("found dir with test files: " + filesDir.getAbsolutePath());
        } else {
            filesDir.mkdir();
            String filesCnt = "";
            String size = "";
            Console console = System.console();
            if (console != null) {
                System.out.println("Enter number of files (default 10)");
                filesCnt = console.readLine();

                System.out.println("Enter size of file in mb (default 5)");
                size = console.readLine();
            }
            try {
                FilesGenerator.generateFiles(Integer.parseInt(filesCnt), Integer.parseInt(size));
            } catch (NumberFormatException e) {
                FilesGenerator.generateFiles(10, 5);
            }
        }
    }

    @SneakyThrows
    public static void generateFiles(Integer filesCnt, Integer fileSizeMb) {
        for (int i = 0; i < filesCnt; i++) {
            generateFile(i, "utf-8", fileSizeMb);
        }
    }

    @SneakyThrows
    private static void generateFile(Integer fileIndex, String encoding, Integer fileSizeMb) {
        Random random = new Random();
        String path = Paths.get(".", "files", fileIndex + ".txt").toAbsolutePath().normalize().toString();
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), encoding))) {
            int size = 0;
            do {
                String[] numbers = new String[1000];
                for (int i = 0; i < 1000; i++) {
                    numbers[i] = random.nextInt(1_000_000) + "";
                }

                String joined = String.join(",", numbers);
                writer.write(joined);
                size += joined.getBytes(encoding).length;
            } while (size < 1024 *1024 * fileSizeMb);
            writer.write("," + (MARK + fileIndex) + "");
        }
        System.out.println("generated " + path);
    }


}
