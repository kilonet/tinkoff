package ru.tinkoff.test.scanners;

import java.io.Reader;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

/**
 * Scans character stream presented by {@link  NumberScanner#reader} for occurrence of {@link NumberScanner#number}
 * @author pokrevsky-a-d
 *         date 18.09.2018
 */
@AllArgsConstructor
class NumberScanner {

    private Reader reader;

    private Integer number;

    private Integer bufferSize;

    public NumberScanner(Reader reader, Integer number) {
        this(reader, number, 1024);
    }

    /**
     *
     * @return true if stream contains char sequence equal to string representation of number, else otherwise
     */
    @SneakyThrows
    public boolean find() {
        char[] buffer = new char[bufferSize];
        String tail = "";
        boolean formatOk = false;
        while (this.reader.read(buffer) > 0) {
            String s = tail + new String(buffer).trim();
            if (!formatOk) {
                formatOk = isFormatOk(s);
                if (!formatOk) {
                    return false;
                }
            }
            if (s.endsWith(",")) {
                tail = "";
            } else {
                int tailIndex = s.lastIndexOf(",");
                if (tailIndex != -1) {
                    tail = s.substring(tailIndex + 1).trim();
                    s = s.substring(0, tailIndex);
                }
            }

            if (s.equals(number.toString()) ||
                s.startsWith(number + ",") || // "222,"
                s.contains("," + number + ",") || //",222,"
                s.endsWith("," + number) // ",222"
                ) {
                return true;
            }

            buffer = new char[bufferSize];
        }
        return tail.equals(number.toString());
    }

    private boolean isFormatOk(String s) {
        for (int i = 0; i < Math.min(s.length(), 32); i++) {
            char c = s.charAt(i);
            if (!Character.isDigit(c) && c != ',')
                return false;
        }
        return true;
    }

}
