package ru.tinkoff.test.scanners;

import java.io.File;
import java.io.FileReader;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

/**
 * Scans text {@link FileScanner#file} content for occurrence of {@link FileScanner#number}
 * @author pokrevsky-a-d
 *         date 18.09.2018
 */
@AllArgsConstructor
public class FileScanner {

    private File file;

    private Integer number;

    /**
     *
     * @return true if file contains char sequence equal to string representation of number, else otherwise
     */
    @SneakyThrows
    public boolean find() {
        try (FileReader reader = new FileReader(this.file)) {
            NumberScanner numberScanner = new NumberScanner(reader, this.number);
            return numberScanner.find();
        }
    }

}
