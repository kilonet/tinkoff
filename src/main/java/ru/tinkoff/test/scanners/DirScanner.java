package ru.tinkoff.test.scanners;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import lombok.AllArgsConstructor;

/**
 * Scans text files in {@link DirScanner#dir folder} (do not scan subdirectories) for files containing {@link DirScanner#number}
 * @author pokrevsky-a-d
 *         date 18.09.2018
 */
@AllArgsConstructor
public class DirScanner {

    private Integer number;

    private File dir;

    /**
     * @return list of file names containing number
     */
    public Collection<String> fileNames() {
        Collection<String> fileNames = new ArrayList<>();
        if (!dir.isDirectory()) {
            throw new IllegalArgumentException("path must be directory");
        }
        for (File f: dir.listFiles()) {
            if (f.isFile() && new FileScanner(f, this.number).find()) {
                fileNames.add(f.getName());
            }
        }
        return fileNames;
    }

}
