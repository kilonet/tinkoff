package ru.tinkoff.test.ws;

import java.io.File;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import ru.tinkoff.test.FindNumberRequest;
import ru.tinkoff.test.FindNumberResponse;
import ru.tinkoff.test.scanners.DirScanner;


/**
 * @author pokrevsky-a-d
 *         date 17.09.2018
 */
@Endpoint
@Slf4j
public class NumbersEndpoint {

    private static final String NAMESPACE_URI = "http://tinkoff.ru/test";

    @Autowired
    private JdbcTemplate db;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "findNumberRequest")
    @ResponsePayload
    public FindNumberResponse findNumber(@RequestPayload FindNumberRequest req) {
        FindNumberResponse resp = new FindNumberResponse();
        try {
            Collection<String> files = new DirScanner(req.getNumber().intValue(), new File("files")).fileNames();
            if (files.size() > 0) {
                resp.getFileNames().addAll(files);
                resp.setCode(SearchResultType.OK.getCode());
            } else {
                resp.setCode(SearchResultType.NOT_FOUND.getCode());
            }
        } catch (Exception e) {
            resp.setCode(SearchResultType.ERROR.getCode());
            resp.setError(e.getMessage());
            log.error("error processing request", e);
        }

        logToDb(req, resp);

        log.info("number: {}, result: {} ", req.getNumber(), resp.getCode());

        return resp;
    }

    private void logToDb(FindNumberRequest request, FindNumberResponse resp) {
        try {
            db.update("INSERT INTO queries (CODE, NUMBER, FILENAMES, ERROR) VALUES (?, ?, ?, ?)",
                    resp.getCode(), request.getNumber(), String.join(",", resp.getFileNames()),
                    resp.getError());
        } catch (Exception e) {
            log.error("error logging to db", e);
        }
    }

    @AllArgsConstructor
    public enum SearchResultType {
        OK("00.Result.OK"), NOT_FOUND("01.Result.NotFound"), ERROR("02.Result.Error");

        @Getter
        private String code;

    }
}
