package ru.tinkoff.test.ws;

import java.util.Locale;

import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.AbstractEndpointExceptionResolver;
import org.springframework.ws.soap.SoapBody;
import org.springframework.ws.soap.SoapMessage;

import lombok.extern.slf4j.Slf4j;

/**
 * @author pokrevsky-a-d
 */
@Slf4j
public class CustomExceptionResolver extends AbstractEndpointExceptionResolver {

    @Override
    protected boolean resolveExceptionInternal(MessageContext messageContext, Object endpoint, Exception ex) {
        SoapMessage response = (SoapMessage) messageContext.getResponse();
        SoapBody body = response.getSoapBody();
        body.addServerOrReceiverFault("Internal error", Locale.ENGLISH);
        log.error("error processing request", ex);
        return true;
    }
}
