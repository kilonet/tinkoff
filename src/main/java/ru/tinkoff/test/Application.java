package ru.tinkoff.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.tinkoff.test.utils.FilesGenerator;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {

        FilesGenerator.generateTestData();

        SpringApplication.run(Application.class, args);
	}
}
