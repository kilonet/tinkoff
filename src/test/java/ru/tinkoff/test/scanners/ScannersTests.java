package ru.tinkoff.test.scanners;

import java.io.File;
import java.io.StringReader;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;

import ru.tinkoff.test.utils.FilesGenerator;

public class ScannersTests {

	@Test
	public void testFind() {
        Assert.assertTrue(new NumberScanner(new StringReader("11,22,33"), 11, "11".length()).find());
        Assert.assertTrue(new NumberScanner(new StringReader("11,22,33"), 22, "11,2".length()).find());
        Assert.assertTrue(new NumberScanner(new StringReader("11,22,33"), 33, 20).find());
        Assert.assertTrue(new NumberScanner(new StringReader("11,22,33,44"), 44, 8).find());
        Assert.assertFalse(new NumberScanner(new StringReader("11,22222,33,44"), 22,  "11,22".length()).find());
	}

    @Test
    public void testSpeed() {
        long time = System.nanoTime();
        boolean find = new FileScanner(new File("files\\1.txt"), FilesGenerator.MARK + 1).find();
        System.out.println(TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - time));
        Assert.assertTrue(find);
    }

    @Test
    public void scanDir() {
        Collection<String> strings = new DirScanner(FilesGenerator.MARK + 9, new File("files")).fileNames();
        Assert.assertTrue(strings.contains("9.txt"));
    }

}
