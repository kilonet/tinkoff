package ru.tinkoff.test;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author pokrevsky-a-d
 *         date 18.09.2018
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class EndpointTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @SneakyThrows
    public void greetingShouldReturnDefaultMessage() {
        String soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:test=\"http://tinkoff.ru/test\"><soapenv:Header/><soapenv:Body><test:findNumberRequest><test:number>10000005</test:number></test:findNumberRequest></soapenv:Body></soapenv:Envelope>";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_XML);
        HttpEntity<String> entity = new HttpEntity<>(soapRequest, headers);
        ResponseEntity<String> soapResp = restTemplate.exchange("http://localhost:" + port + "/ws", HttpMethod.POST, entity, String.class);

        Assert.assertTrue(soapResp.getBody().contains("<ns2:findNumberResponse xmlns:ns2=\"http://tinkoff.ru/test\"><ns2:code>00.Result.OK</ns2:code><ns2:fileNames>5.txt</ns2:fileNames></ns2:findNumberResponse>"));
    }

}
