### Run Tests
```
mvn test
```
(test data will be generated, see console, test require at least 10 files)

### Run application
binary located in /dist folder
```
java -jar scanner.jar
```
will run on localhost:8080  
see console to generate test data  
DB can be checked at localhost:8080/h2/   
jdbc:h2:mem:scanner , sa, empty password  